//
//  ViewController.h
//  FiltersApp
//
//  Created by Pavel Gurov on 03/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface ImageViewController : CommonViewController

@end