//
//  CommonViewController.m
//  FiltersApp
//
//  Created by Pavel Gurov on 05/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController ()

@end

@implementation CommonViewController

+ (NSString *)storyboardID
{
    return NSStringFromClass([self class]);
}

- (void)dealloc
{
    NSLog(@"[%@ dealloc]", self);
    [self methodCalledFromDealloc];
}

- (void)methodCalledFromDealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
