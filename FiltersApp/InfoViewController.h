//
//  InfoViewController.h
//  FiltersApp
//
//  Created by Pavel Gurov on 04/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface InfoViewController : CommonViewController

@end
