//
//  AppDelegate.h
//  FiltersApp
//
//  Created by Pavel Gurov on 03/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
