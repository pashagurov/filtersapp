//
//  ToneCurveFilterViewController.m
//  FiltersApp
//
//  Created by Pavel Gurov on 05/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import "ToneCurveFilterViewController.h"

@interface ToneCurveFilterViewController ()

@property (weak, nonatomic) IBOutlet UIView *slidersContainer;
@property (nonatomic, strong) IBOutletCollection(UISlider) NSArray *sliders;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic) NSInteger selectedSegmentIndex;
@property (strong, nonatomic) NSArray *controlPoints;
@property (strong, nonatomic) GPUImageToneCurveFilter *toneCurveFilter;

- (IBAction)segmentPicked:(UISegmentedControl *)sender;
- (IBAction)sliderValueChanged:(UISlider *)sender;
- (IBAction)sliderTouchUpInside:(UISlider *)sender;
- (IBAction)sliderTouchUpOutside:(UISlider *)sender;
- (IBAction)resetTap:(UIButton *)sender;

@end

@implementation ToneCurveFilterViewController

- (void)initConfig
{
    [super initConfig];
    [self resetPoints];
    
    self.toneCurveFilter = [[GPUImageToneCurveFilter alloc] init];
    self.filter = self.toneCurveFilter;
    
    [self updateFilter];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UISlider *aSlider in self.sliders) {
        [aSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [aSlider addTarget:self action:@selector(sliderTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [aSlider addTarget:self action:@selector(sliderTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    }
    
    [self.slidersContainer setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
    [self updateSliders];
}

//------------------------------------------------------------------------------
#pragma mark - Helpers

- (void)resetPoints
{
    NSMutableArray *allControlPointsArray = [NSMutableArray arrayWithCapacity:4];
    for (int i=0; i<4; i++) {
        
        NSMutableArray *controlPoints = [NSMutableArray arrayWithCapacity:8];
        for (int j=0; j<8; j++) {
            
            CGFloat coord = j/7.;
            CGPoint newPoint = CGPointMake(coord, coord);
            [controlPoints addObject:[NSValue valueWithCGPoint:newPoint]];
        }
        [allControlPointsArray addObject:controlPoints];
    }
    self.controlPoints = [allControlPointsArray copy];
    
    [self updateSliders];
    [self updateFilter];
}

//----------------------------------------------------------------------------------------------------
#pragma mark - View refreshing

- (void)updateSliders
{
    NSArray *controlPoints = self.controlPoints[self.selectedSegmentIndex];
    
    for (int i=0; i<self.sliders.count; i++) {
        UISlider *slider = self.sliders[i];
        NSInteger sliderTag = slider.tag;
        if (controlPoints.count>sliderTag) {
            CGPoint point = [controlPoints[sliderTag] CGPointValue];
            [slider setValue:point.y];
        }
    }
}

//------------------------------------------------------------------------------
#pragma mark  - user actions

- (IBAction)segmentPicked:(UISegmentedControl *)sender
{
    self.selectedSegmentIndex = sender.selectedSegmentIndex;
    [self updateSliders];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
//    [self updateControlPointsForSlider:sender];
}

- (IBAction)sliderTouchUpInside:(UISlider *)sender
{
    [self updateControlPointsForSlider:sender];
}

- (IBAction)sliderTouchUpOutside:(UISlider *)sender
{
    [self updateControlPointsForSlider:sender];
}

- (void)updateControlPointsForSlider:(UISlider *)slider
{
    NSMutableArray *controlPoints = self.controlPoints[self.selectedSegmentIndex];
    CGPoint sliderPoint = [controlPoints[slider.tag] CGPointValue];
    sliderPoint.y = slider.value;
    [controlPoints setObject:[NSValue valueWithCGPoint:sliderPoint] atIndexedSubscript:slider.tag];
    
    [self updateFilter];
}

- (IBAction)resetTap:(UIButton *)sender
{
    [self resetPoints];
}

//------------------------------------------------------------------------------
#pragma mark - Image processing

- (void)updateFilter
{
    if (self.filter) {
        
        [self.toneCurveFilter setRedControlPoints:self.controlPoints[0]];
        [self.toneCurveFilter setGreenControlPoints:self.controlPoints[1]];
        [self.toneCurveFilter setBlueControlPoints:self.controlPoints[2]];
        [self.toneCurveFilter setRgbCompositeControlPoints:self.controlPoints[3]];
        
        [self.filerDelegate filterUpdated:self.filter];
    }
}

@end