//
//  HSLFilterViewController.m
//  FiltersApp
//
//  Created by Pavel Gurov on 27/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import "HSLFilterViewController.h"
#import "SaturationHueLuminanceFilter.h"

@interface HSLFilterViewController ()
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (nonatomic, strong) IBOutletCollection(UISlider) NSArray *sliders;
@property (weak, nonatomic) IBOutlet UISlider *hueSlider;
@property (weak, nonatomic) IBOutlet UISlider *saturationSlider;
@property (weak, nonatomic) IBOutlet UISlider *luminanceSlider;

@property (weak, nonatomic) IBOutlet UILabel *labelHueValue;
@property (weak, nonatomic) IBOutlet UILabel *labelSaturationValue;
@property (weak, nonatomic) IBOutlet UILabel *labelLuminanceValue;

@property (weak, nonatomic) IBOutlet UIView *viewSlidersContainer;

@property (strong, nonatomic) SaturationHueLuminanceFilter *hslFilter;

- (IBAction)segmentPicked:(UISegmentedControl *)sender;
- (IBAction)sliderValueChanged:(UISlider *)sender;
- (IBAction)sliderTouchUpInside:(UISlider *)sender;
- (IBAction)sliderTouchUpOutside:(UISlider *)sender;
- (IBAction)resetTap:(UIButton *)sender;

@property (nonatomic) GLfloat *hueArray;
@property (nonatomic) GLfloat *saturationArray;
@property (nonatomic) GLfloat *luminanceArray;
@end

@implementation HSLFilterViewController

- (void)initConfig
{
    [super initConfig];
    
    self.hueArray = malloc(sizeof(GLfloat) * 6);
    self.saturationArray = malloc(sizeof(GLfloat) * 6);
    self.luminanceArray = malloc(sizeof(GLfloat) * 6);
    [self resetPoints];
    
    self.hslFilter = [[SaturationHueLuminanceFilter alloc] init];
    [self.hslFilter forceProcessingAtSize:self.gpuImageSize];
    
    self.filter = self.hslFilter;
    
    [self updateFilter];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UISlider *aSlider in self.sliders) {
        [aSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [aSlider addTarget:self action:@selector(sliderTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [aSlider addTarget:self action:@selector(sliderTouchUpOutside:) forControlEvents:UIControlEventTouchUpOutside];
    }
    
    [self.viewSlidersContainer setTransform:CGAffineTransformMakeRotation(-M_PI_2)];
    [self updateSliders];
}

//------------------------------------------------------------------------------
#pragma mark - Helpers

- (void)resetPoints
{
    for (int i=0; i<6; i++) {
        self.hueArray[i] = (GLfloat) 0.5f;
        self.saturationArray[i] = (GLfloat) 0.5f;
        self.luminanceArray[i] = (GLfloat) 0.5f;
    }

    [self updateSliders];
    [self updateFilter];
}

//----------------------------------------------------------------------------------------------------
#pragma mark - View refreshing

- (void)updateSliders
{
    [self.hueSlider setValue:self.hueArray[self.segmentedControl.selectedSegmentIndex]];
    [self.saturationSlider setValue:self.saturationArray[self.segmentedControl.selectedSegmentIndex]];
    [self.luminanceSlider setValue:self.luminanceArray[self.segmentedControl.selectedSegmentIndex]];

    [self updateValueLabels];
}

- (void)updateValueLabels
{
    [self.labelHueValue setText:[NSString stringWithFormat:@"%.3f", self.hueSlider.value]];
    [self.labelSaturationValue setText:[NSString stringWithFormat:@"%.3f", self.saturationSlider.value]];
    [self.labelLuminanceValue setText:[NSString stringWithFormat:@"%.3f", self.luminanceSlider.value]];
}

//------------------------------------------------------------------------------
#pragma mark  - user actions

- (IBAction)segmentPicked:(UISegmentedControl *)sender
{
    [self updateSliders];
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
//    [self updateControlPointsForSlider:sender];
}

- (IBAction)sliderTouchUpInside:(UISlider *)sender
{
    [self updateControlPointsForSlider:sender];
}

- (IBAction)sliderTouchUpOutside:(UISlider *)sender
{
    [self updateControlPointsForSlider:sender];
}

- (void)updateControlPointsForSlider:(UISlider *)slider
{
    if (slider.tag == 0) { // hue
        self.hueArray[self.segmentedControl.selectedSegmentIndex] = (GLfloat)slider.value;
    }
    else if (slider.tag == 1) { // saturation
        self.saturationArray[self.segmentedControl.selectedSegmentIndex] = (GLfloat)slider.value;
    }
    else if (slider.tag == 2) { // luminance
        self.luminanceArray[self.segmentedControl.selectedSegmentIndex] = (GLfloat)slider.value;
    }
    
    [self updateValueLabels];
    [self updateFilter];
}

- (IBAction)resetTap:(UIButton *)sender
{
    [self resetPoints];
}

//------------------------------------------------------------------------------
#pragma mark - Image processing

- (void)updateFilter
{
    if (self.filter) {
        
        [self.hslFilter setHue:self.hueArray];
        [self.hslFilter setSaturation:self.saturationArray];
        [self.hslFilter setLuminance:self.luminanceArray];

        [self.filerDelegate filterUpdated:self.filter];
    }
}

@end