//
//  CommonViewController.h
//  FiltersApp
//
//  Created by Pavel Gurov on 05/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonViewController : UIViewController

+ (NSString *)storyboardID;

@end