//
//  CommonFilterViewController.h
//  FiltersApp
//
//  Created by Pavel Gurov on 05/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import "CommonViewController.h"

@protocol FilterDelegate;

@interface CommonFilterViewController : CommonViewController

- (void)initConfig;

@property (nonatomic) CGSize gpuImageSize;
@property (strong, nonatomic) GPUImageFilter *filter;
@property (nonatomic) NSInteger controllerIndex;

@property (weak, nonatomic) id<FilterDelegate> filerDelegate;

@end

@protocol FilterDelegate <NSObject>
@required
- (void)filterUpdated:(GPUImageFilter *)filter;
@end