//
//  GeneralHelper.h
//  FiltersApp
//
//  Created by Pavel Gurov on 03/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneralHelper : NSObject

+ (void)showFrameOfView:(UIView *)view withColor:(UIColor *)color;

@end

@interface UIImage (fixOrientation)
- (UIImage *)fixOrientation;
@end