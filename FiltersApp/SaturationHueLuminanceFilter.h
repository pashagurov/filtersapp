#import <GPUImage.h>

@interface SaturationHueLuminanceFilter : GPUImageFilter
{
    GLfloat referenceTones[8]; // Blue and Aqua are included twice to manage edge cases
}

// Accept an array on 6 floats from -1.0 to 1.0 for Red, Yellow, Green, Aqua, Blue and Magenta respectively
@property(nonatomic) GLfloat* saturation;
@property(nonatomic) GLfloat* hue;
@property(nonatomic) GLfloat* luminance;

@end
