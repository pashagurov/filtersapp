//
//  ViewController.m
//  FiltersApp
//
//  Created by Pavel Gurov on 03/07/14.
//  Copyright (c) 2014 Superbright. All rights reserved.
//

#import "ImageViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CommonFilterViewController.h"
#import "ToneCurveFilterViewController.h"
#import "HSLFilterViewController.h"

@interface ImageViewController () <UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource, FilterDelegate> {
    
    dispatch_once_t onceToken;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProcessedImage;
@property (weak, nonatomic) IBOutlet UIView *viewPagesContainer;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *buttonPickPhoto;
@property (weak, nonatomic) IBOutlet UIButton *buttonSave;

@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) UIPageViewController *pageViewController;

// filters
@property (strong, nonatomic) NSMutableArray *viewControllers;
@property (strong, nonatomic) ToneCurveFilterViewController *toneCurveFilterVC;
@property (strong, nonatomic) HSLFilterViewController *hslFilterVC;
@property (strong, nonatomic) GPUImageFilterGroup *filtersGroup;
@property (strong, nonatomic) GPUImagePicture *sourceImage;

- (IBAction)photoTap:(UIButton *)sender;
- (IBAction)saveTap:(UIButton *)sender;

@end

@implementation ImageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    dispatch_once(&onceToken, ^{
        
        [self.buttonPickPhoto setImage:nil forState:UIControlStateNormal];
        [self.buttonSave setEnabled:YES];

    // filters controllers
        self.viewControllers = [NSMutableArray array];
        
    // tone curves
        self.toneCurveFilterVC = [self.storyboard instantiateViewControllerWithIdentifier:[ToneCurveFilterViewController storyboardID]];
        [self.toneCurveFilterVC initConfig];
        self.toneCurveFilterVC.filerDelegate = self;
        self.toneCurveFilterVC.controllerIndex = 0;
        [self.viewControllers addObject:self.toneCurveFilterVC];
        
    // HSL
        self.hslFilterVC = [self.storyboard instantiateViewControllerWithIdentifier:[HSLFilterViewController storyboardID]];
        [self.hslFilterVC initConfig];
        self.hslFilterVC.filerDelegate = self;
        self.hslFilterVC.controllerIndex = 1;
        [self.viewControllers addObject:self.hslFilterVC];
        
        [self.pageControl setNumberOfPages:self.viewControllers.count];
        
    // pages VC
        self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                                  navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                                options:nil];
        
        self.pageViewController.dataSource = self;
        self.pageViewController.delegate = self;
        
        [[self.pageViewController view] setFrame:self.viewPagesContainer.bounds];
        
        [self addChildViewController:self.pageViewController];
        [self.viewPagesContainer insertSubview:[self.pageViewController view] atIndex:0];
        
        [self.pageViewController setViewControllers:[NSArray arrayWithObject:self.toneCurveFilterVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
        }];

    // initial source image
        self.sourceImage = [[GPUImagePicture alloc] initWithImage:[UIImage imageNamed:@"colormap"]];
    });
}

//------------------------------------------------------------------------------
#pragma mark - Setters

- (void)setSourceImage:(GPUImagePicture *)sourceImage
{
    _sourceImage = sourceImage;
    [self processImage];
}

//----------------------------------------------------------------------------------------------------
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0: {
            // make photo
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                
                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                                      message:@"Камера недоступна"
                                                                     delegate:nil
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles: nil];
                [myAlertView show];
            }
            else {
                [self takePhotoForSource:UIImagePickerControllerSourceTypeCamera];
            }
            break;
        }
        case 1: {
            // choose photo
            [self takePhotoForSource:UIImagePickerControllerSourceTypePhotoLibrary];
            break;
        }
        default:
            break;
    }
}

//=--------------------------------------------------------------------------------------------------
#pragma mark - UIPagesViewController DataSource & Delegate

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index<self.viewControllers.count) {
        return self.viewControllers[index];
    }
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if (!self.viewControllers.count) {
        return nil;
    }
    
    NSUInteger index = [(CommonFilterViewController *)viewController controllerIndex];
    if (index == 0) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if (!self.viewControllers.count) {
        return nil;
    }
    
    NSUInteger index = [(CommonFilterViewController *)viewController controllerIndex];
    if (index == self.viewControllers.count-1) {
        return nil;
    }
    
    index++;
    return [self viewControllerAtIndex:index];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        CommonFilterViewController *currentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
        NSInteger pageIndex = currentViewController.controllerIndex;
        
        [self.pageControl setCurrentPage:pageIndex];
    }
}


//------------------------------------------------------------------------------
#pragma mark  - user actions

- (IBAction)photoTap:(UIButton *)sender
{
    // show action sheet
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:@"Отмена"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:
                            @"Сделать фото",
                            @"Выбрать фото",
                            nil];
    
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)saveTap:(UIButton *)sender
{
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//
//    [self.filtersGroup useNextFrameForImageCapture];
//    [self.sourceImage processImage];
//    UIImage *image = [self.filtersGroup imageFromCurrentFramebuffer];
//    
//    [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
//        
//        if (error) {
//            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не удалось сохранить изображение" delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
//        } else {
//            [[[UIAlertView alloc] initWithTitle:@"Сохранено" message:@"Изображение сохранено в ваши фотографии" delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
//        }
//    }];
}

//------------------------------------------------------------------------------
#pragma mark - ReportItemEditablePhotoCellDelegate

- (void)takePhotoForSource:(UIImagePickerControllerSourceType)sourceType
{
    self.imagePickerController = [[UIImagePickerController alloc] init];
    
    self.imagePickerController.sourceType = sourceType;
    self.imagePickerController.delegate = self;
    self.imagePickerController.allowsEditing = NO;
    
    [self presentViewController:self.imagePickerController animated:YES completion:^{ }];
}

//------------------------------------------------------------------------------
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.imagePickerController.presentingViewController dismissViewControllerAnimated:YES completion:^{

        UIImage *pickedImage = info[UIImagePickerControllerOriginalImage];
        
        if (pickedImage) {
            
            pickedImage = [pickedImage fixOrientation];
            self.sourceImage = [[GPUImagePicture alloc] initWithImage:pickedImage];
            
            [self.buttonPickPhoto setImage:nil forState:UIControlStateNormal];
            [self.buttonSave setEnabled:YES];
        }
    }];
}

//------------------------------------------------------------------------------
#pragma mark - FilterDelegate

- (void)filterUpdated:(GPUImageFilter *)filter
{
    [self processImage];
}


- (void)processImage
{
    if (self.sourceImage) {
        
//        // manage group
//        self.filtersGroup = [[GPUImageFilterGroup alloc] init];
//        [self.filtersGroup addFilter:self.toneCurveFilterVC.filter];
//        [self.filtersGroup addFilter:self.hslFilterVC.filter];
//        
//        // chain those fuckers up
//        [self.toneCurveFilterVC.filter addTarget:self.hslFilterVC.filter];
//        [self.filtersGroup setInitialFilters:[NSArray arrayWithObject:self.toneCurveFilterVC.filter]];
//        [self.filtersGroup setTerminalFilter:self.hslFilterVC.filter];
        
        // filters chained manually
        [self.sourceImage addTarget:self.hslFilterVC.filter];
        [self.hslFilterVC.filter useNextFrameForImageCapture];
        [self.hslFilterVC.filter addTarget:self.toneCurveFilterVC.filter];
        [self.toneCurveFilterVC.filter useNextFrameForImageCapture];
        [self.sourceImage processImage];
        [self.imageViewProcessedImage setImage:[self.toneCurveFilterVC.filter imageFromCurrentFramebuffer]];

//        // do the processing
        
#warning THIS DOESNT FCKN WORK!
//        [self.sourceImage addTarget:self.hslFilterVC.filter];
//        [self.hslFilterVC.filter useNextFrameForImageCapture];
//        [self.sourceImage processImage];
//        [self.imageViewProcessedImage setImage:[self.hslFilterVC.filter imageFromCurrentFramebuffer]];
        
#warning THIS ACTUALLY DOES, but only on 64bit iphone5S
//        [self.sourceImage addTarget:self.toneCurveFilterVC.filter];
//        [self.toneCurveFilterVC.filter useNextFrameForImageCapture];
//        [self.sourceImage processImage];
//        [self.imageViewProcessedImage setImage:[self.toneCurveFilterVC.filter imageFromCurrentFramebuffer]];
    }
}

@end